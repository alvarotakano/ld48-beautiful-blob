﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    public Transform player;
    Animator anim;
    public AudioSource levelUp;
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        levelUp = this.GetComponent<AudioSource>();
        anim.SetBool("Level1", false);
        anim.SetBool("Level2", false);
    }
    void playClip()
    {
        levelUp.Play();
    }
    // Update is called once per frame
    void Update()
    {
        if (player.position.y < 8 && anim.GetBool("Level1") == false)
        {
            anim.SetBool("Level1", true);
            Invoke("playClip", 2.4f);
        }
        if (player.position.y < -6 && anim.GetBool("Level2") == false)
        {
            anim.SetBool("Level2", true);
            Invoke("playClip", 2.4f);
        }
    }
}
