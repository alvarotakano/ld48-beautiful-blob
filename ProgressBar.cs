﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] Transform playerProgress;
    [SerializeField] Transform beginning;
    [SerializeField] Transform end;
    [SerializeField] float travelDistance;
    [SerializeField] AudioSource song;
    Image img;
    // Start is called before the first frame update
    void Start()
    {
        img = this.GetComponent<Image>();
        travelDistance = end.position.y - beginning.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        img.fillAmount = (travelDistance-(end.position.y - playerProgress.position.y)) / travelDistance;
        //update song pitch dependeing on progress
        float pitch = (end.position.y - playerProgress.position.y) / travelDistance;
        song.pitch = Mathf.Clamp(pitch, 0.5f, 1f);
        
    }
}
