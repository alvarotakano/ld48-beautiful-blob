﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float turnSpeed;
    public float startSpeed = 0;
    public float descendingSpeed;
    public float minX, maxX;
    public float minCamX, maxCamX;
    public float minCamY, maxCamY;
    public GameOver GameOver;
    public Image progressBar;
    public GameObject sprite;
    public AudioSource playerLose;
    public AudioSource playerWin;
    [SerializeField] GameObject cam;
    [SerializeField] GameObject spawn;
    [SerializeField] Transform end;
    private Vector3 mousePos;
    bool win = false;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = spawn.transform.position;
        playerLose = this.GetComponent<AudioSource>();
        playerWin = this.GetComponent<AudioSource>();
    }
    public void GameOverScreen()
    {
        GameOver.Setup(progressBar.fillAmount * 100);
    }
    // Update is called once per frame
    void Update()
    {
        //Input of mouse
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.x = Mathf.Clamp(mousePos.x, minX, maxX);
        mousePos.y = transform.position.y;
        mousePos.z = transform.position.z;
        //Move gradually towards an x value of screen
        transform.position = Vector3.Lerp(transform.position, mousePos, turnSpeed * Time.deltaTime);
        //Constant downwards movement
        transform.position = transform.position - Vector3.up*descendingSpeed*Time.deltaTime;
        //Block how far the camera can go
        Vector3 camPos = new Vector3(cam.transform.position.x, cam.transform.position.y, cam.transform.position.z);
        camPos.x = Mathf.Clamp(cam.transform.position.x, minCamX, maxCamX);
        camPos.y = Mathf.Clamp(cam.transform.position.y, minCamY, maxCamY);
        cam.transform.position = camPos;
        //Make the player lean towards where he is going
        Vector3 lookPos = new Vector3(mousePos.x, transform.position.y-5, mousePos.z);
        lookPos = lookPos - transform.position;
        float angle = (Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg)+90;
        sprite.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        if(transform.position.y <= end.position.y&&win==false)
        {
            playerWin.Play();
            descendingSpeed = 0;
            turnSpeed = 0;
            sprite.transform.rotation = new Quaternion(0,0,0,0);
            Invoke("GameOverScreen", 1);
            win = true;
        }
    }
}
