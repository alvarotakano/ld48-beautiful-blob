﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOver : MonoBehaviour
{
    public TMP_Text gameOver;
    public TMP_Text pointsText;
    public void Setup(float score)
    {
        gameObject.SetActive(true);
        pointsText.text = score.ToString("0") + "% of true beauty";
        if(score>99)
        {
            gameOver.text = "You won!";
            gameOver.color = Color.yellow;
        }
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(0);
    }
}
