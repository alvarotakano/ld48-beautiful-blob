﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleSpawner : MonoBehaviour
{
    [SerializeField] GameObject bubble;
    [SerializeField] List<Transform> spawns = new List<Transform>();
    [SerializeField] float minTime;
    [SerializeField] float maxTime;
    // Start is called before the first frame update
    private bool isSpawning;

    void Awake()
    {
        isSpawning = false;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isSpawning)
        {
            float timer = Random.Range(minTime, maxTime);
            Invoke("SpawnObject", timer);
            isSpawning = true;
        }
        
    }

    void SpawnObject()
    {
        Vector3 randomZ = new Vector3(spawns[(int)Random.Range(0, spawns.Count)].transform.position.x, spawns[(int)Random.Range(0, spawns.Count)].transform.position.y, Random.Range(-1.9f, 0.1f));
        Instantiate(bubble, randomZ, bubble.transform.rotation);
        float randomSize = Random.Range(0.3f, 1.2f);
        bubble.transform.localScale = new Vector3(randomSize, randomSize, 1); 
        isSpawning = false;
    }
}
