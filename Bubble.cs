﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour
{
    [SerializeField] float floatingSpeed;
    [SerializeField] float bubbleTurnSpeed;
    [SerializeField] float bubbleSpread;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Update()
    {   
        Vector3 pos = new Vector3(transform.position.x + Mathf.Sin(Time.time*bubbleTurnSpeed)*bubbleSpread, transform.position.y + floatingSpeed * Time.deltaTime, transform.position.z);
        transform.position = pos;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "bubbleDestroyer")
        {
            Destroy(gameObject);
        }
    }
}
